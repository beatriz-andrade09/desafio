# Desafio Backend - WebJump

# Objetivo 
- Criação de um projeto capaz de inserir, ler, atualizar e apagar dados de categorias e produtos.

# Tecnologias  Utilizadas 
- Linguagem PHP;
- IDE Visual Studio Code (v1.62.3) e extensão PHP Intelephense (v1.7.1);
- Wampserver (v3.2.3);
- MySQL (v5.7.31);
- phpMyAdmin (v5.0.2);

# Execução do projeto
- Instalação do sevidor Wampserver;
- Criação do Banco de Dados MYSQL com phpMyAdmin -> script na pasta "script - banco desafiowebjump";
- Instalação da IDE e extensão;
- Criação de classe para a conexão com o banco;
- Criação de classes categoria, produto e seus respectivos CRUD;
- Utilização dos arquivos html e css disponíveis no diretório assets para a execução do CRUD.

