<?php
class ProductDAO {
     public function create(Product $p) {
          $sql = "INSERT INTO product (sku, name, price, quantity, description, code_category, image) VALUES (?, ?, ?, ?, ?,?,?);";   
          $stmt = Conexao::getConn() -> prepare ($sql);
          $stmt -> bindValue(1, $p -> getSku());
          $stmt -> bindValue(2, $p -> getName());
          $stmt -> bindValue(3, $p -> getPrice());
          $stmt -> bindValue(4, $p -> getQuantity());
          $stmt -> bindValue(5, $p -> getDescription());
          $stmt -> bindValue(6, $p -> getCategory());
          $stmt -> bindValue(7, $p -> getImage());
          $stmt -> execute();
     }

     public function read() {
          $sql = "SELECT * FROM product;";   
          $stmt = Conexao::getConn() -> prepare ($sql);
          $stmt -> execute();
          if($stmt -> rowCount() > 0):
               $result = $stmt -> fetchAll (\PDO::FETCH_ASSOC);
               return $result;
          else: 
               return [];
          endif;
     }

     public function readWithCode($sku) {
          $sql = "SELECT * FROM product where sku = ?;";   
          $stmt = Conexao::getConn() -> prepare ($sql);
          $stmt -> bindValue(1, $sku);
          $stmt -> execute();
          if($stmt -> rowCount() > 0):
               $result = $stmt -> fetchAll (\PDO::FETCH_ASSOC);
               return $result;
          else: 
               return [];
          endif;
     }

     public function readDashboard() {
          $sql = "SELECT sku, price, image, name FROM product ORDER BY sku DESC LIMIT 2;";   
          $stmt = Conexao::getConn() -> prepare ($sql);
          $stmt -> execute();
          if($stmt -> rowCount() > 0):
               $result = $stmt -> fetchAll (\PDO::FETCH_ASSOC);
               return $result;
          else: 
               return [];
          endif;
     }

     public function update($sku, $name) {  
          $sql = "UPDATE product SET name = ? WHERE sku = ?; ";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->bindValue(1, $name);
          $stmt->bindValue(2, $sku);
          $stmt->execute();
     }

     public function delete($sku) {
          $sql = "DELETE FROM product WHERE sku = ?;"; 
          $stmt = Conexao::getConn() -> prepare ($sql);
          $stmt -> bindValue(1, $sku);
          $stmt -> execute();
  
       
     }
}
