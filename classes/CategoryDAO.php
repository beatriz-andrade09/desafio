<?php
class CategoryDAO
{
     public function create(Category $c) {
          $sql = "INSERT INTO category (category_code, category_name) VALUES (?, ?); ";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->bindValue(1, $c->getCode());
          $stmt->bindValue(2, $c->getName());
          $stmt->execute();
     }

     public function read() {
          $sql = "SELECT * FROM category;";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->execute();
          if ($stmt->rowCount() > 0) :
               $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
               return $result;
          else :
               return [];
          endif;
     }

     public function readWithCode($code)  {
          $sql = "SELECT * FROM category where category_code = ?;";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->bindValue(1, $code);
          $stmt->execute();
          if ($stmt->rowCount() > 0) :
               $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
               return $result;
          else :
               return [];
          endif;
     }



     public function update($code, $category_name) {
          $sql = "UPDATE category SET category_name = ? WHERE category_code = ?; ";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->bindValue(1, $category_name);
          $stmt->bindValue(2, $code);
          $stmt->execute();
     }

     public function delete($code)   {
          $sql = "DELETE FROM category WHERE category_code = ?;";
          $stmt = Conexao::getConn()->prepare($sql);
          $stmt->bindValue(1, $code);
          $stmt->execute();
     }
}
