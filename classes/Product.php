<?php
class Product {
    private $name, $sku, $price, $description, $quantity, $category, $image;

    public function getName()   {
        return $this->name;
    }

    public function setName($name)  {
        $this->name = $name;
    }

    public function getSku()  {
        return $this->sku;
    }

    public function setSku($sku)  {
        $this->sku = $sku;
    }


    public function getPrice()  {
        return $this->price;
    }

    public function setPrice($price)  {
        $this->price = $price;
    }


    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description)  {
        $this->description  = $description;
    }

    public function getQuantity()  {
        return $this->quantity;
    }

    public function setQuantity($quantity)  {
        $this->quantity = $quantity;
    }

    public function getCategory()  {
        return $this-> category;
    }

    public function setCategory($category)  {
        $this->category  = $category;
    }

    public function getImage()  {
        return $this-> image;
    }

    public function setImage($image)  {
        $this->image  = $image;
    }
}
