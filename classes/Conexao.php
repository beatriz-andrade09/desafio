<?php
    class Conexao {
        private static $instance;
        public static function getConn () {
            if(!isset(self::$instance)):
                self::$instance = new \PDO ('mysql:dbname=desafiowebjump;host=localhost:3306;charset=utf8', 'root', '');
            endif;
                return self::$instance;
        }
    }
?>