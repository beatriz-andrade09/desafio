<?php

class Category {

    private $name, $code;

    public function getName() {
        return $this->name;
    }

    public function setName($name)  {
        $this->name = $name;
    }

    public function getCode()  {
        return $this->code;
    }

    public function setCode($code) {
        $this-> code = $code;
    }
}
