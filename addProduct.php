<?php

  require 'classes/Conexao.php';
  require 'classes/Product.php';
  require 'classes/ProductDAO.php';

  if (!empty($_POST['sku'])  && !empty($_POST['name']) && !empty($_POST['price']) && !empty($_POST['quantity']) && !empty($_POST['description']) && !empty($_POST['category']) && isset($_FILES['arquivo'])) {

    if ((!is_numeric($_POST['sku'])) || (is_numeric($_POST['name'])) || (!is_numeric($_POST['price']))  || (!is_numeric($_POST['quantity']))  || (is_numeric($_POST['description']))  || (!is_numeric($_POST['category']))) {
      echo 'Preencha os campos corretamente e tente novamente.';
      die();
    }

    $product = new Product();

    $extensao = strtolower(substr($_FILES['arquivo']['name'], -4));
    $novo_nome = md5(time()) . $extensao;
    $diretorio = "images/upload-images/";
    move_uploaded_file($_FILES['arquivo']['tmp_name'], $diretorio . $novo_nome);

    $product->setSku(($_POST['sku']));
    $product->setName(($_POST['name']));
    $product->setPrice(($_POST['price']));
    $product->setQuantity(($_POST['quantity']));
    $product->setDescription(($_POST['description']));
    $product->setCategory(($_POST['category']));
    $product->setImage($novo_nome);

    $productDAO = new ProductDAO();
    $productDAO->create($product);
    header("Refresh: 0; url=products.php");
  }

?>

<!doctype html>
<html ⚡>

<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>
    body {
      -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-moz-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-ms-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-o-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }
  </style><noscript>
    <style amp-boilerplate>
      body {
        -webkit-animation: none;
        -moz-animation: none;
        -ms-animation: none;
        animation: none
      }
    </style>
  </noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
  <h1 class="title new-item">New Product</h1>
  <form name="cad-produto" method="POST" enctype="multipart/form-data">
    <div class="input-field">
      <label for="sku" class="label">Product SKU</label>
      <input type="text" name="sku" required class="input-text" />
    </div>
    <div class="input-field">
      <label for="name" class="label">Product Name</label>
      <input type="text" name="name" required class="input-text" />
    </div>
    <div class="input-field">
      <label for="price" class="label">Price</label>
      <input type="text" name="price" required class="input-text" />
    </div>
    <div class="input-field">
      <label for="category" class="label">Category</label>
      <input type="text" name="category" required class="input-text" />
    </div>
    <div class="input-field">
      <label for="quantity" class="label">Quantity</label>
      <input type="text" name="quantity" required class="input-text" />
    </div>
    <div class="input-field">
      <label for="arquivo" class="label">Image</label>
      <input type="file" required name="arquivo">
      <br />
    </div>
    <div class="input-field">
      <label for="description" class="label">Description</label>
      <textarea name="description" requerid class="input-text"></textarea>
    </div>
    <div class="actions-form">
      <a href="products.php" class="action back">Back</a>
      <input class="btn-submit btn-action" type="submit" name="save" value="Save Product" />
    </div>

  </form>
</main>
<!-- Main Content -->

<!-- Footer -->
<footer>
  <div class="footer-image">
    <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
  </div>
  <div class="email-content">
    <span>go@jumpers.com.br</span>
  </div>
</footer>
<!-- Footer -->
</body>

</html>